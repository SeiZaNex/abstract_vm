//
// Chipset.hpp for  in /home/zhou_f//c++/abstract_vm
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Mon Feb 11 17:08:27 2013 fong zhou
// Last update Sat Feb 16 19:32:58 2013 lyoma guillou
//

#ifndef CHIPSET_HPP_
# define CHIPSET_HPP_

# include <string>

# include "Operand.hpp"
# include "Cpu.hpp"

namespace IO
{
  enum	eIOFlag
    {
      IOS,
      FILE
    };

  struct		s_conv
  {
    std::string		name;
    Op::eOperandType	type;
  };

  struct		s_cmd
  {
    std::string		cmd;
    unsigned int	type;
    std::string		arg;
    unsigned int	line;
  };

  class Chipset
  {
    Cpu _cpu;
    struct s_cmd cmds;
    
  public:

    Chipset();
    Chipset(Chipset const &);
    ~Chipset();

    Chipset & operator=(Chipset const &);

    bool error_check(struct s_cmd exec);
    Op::eOperandType get_type(std::string & typeName) const;
    bool get_input(std::string & buff, unsigned int & line);
    void get_stream(char const *file_name = NULL);  
    void execute_cmd();
  };
};

#endif
