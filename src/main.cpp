//
// main.cpp for  in /home/zhou_f//c++/abstract_vm
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Mon Feb 11 16:58:26 2013 fong zhou
// Last update Sat Feb 16 19:33:00 2013 lyoma guillou
//

#include "Chipset.hpp"

int main(int ac, char **av)
{
  IO::Chipset chipset;

  if (ac > 1)
    chipset.get_stream(av[1]);
  else
    chipset.get_stream(NULL);
  return 0;
}
