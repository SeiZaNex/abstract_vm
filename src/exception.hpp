//
// exception.hpp for  in /home/zhou_f//c++/abstract_vm
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Mon Feb 11 20:38:18 2013 fong zhou
// Last update Sat Feb 16 10:48:51 2013 fong zhou
//

#ifndef EXCEPTION_HPP_
# define EXCEPTION_HPP_

#include <exception>
#include <string>

class Exception : public std::exception
{
  std::string _e;
  
public:
  Exception(std::string const &) throw();
  Exception(Exception const &) throw();
  virtual ~Exception() throw();

  Exception & operator=(Exception const &);

  virtual const char * what() const throw();
};

#endif
