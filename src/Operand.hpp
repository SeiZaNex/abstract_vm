//
// Operand.hpp for  in /home/zhou_f//c++/abstract_vm/tmp
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Thu Feb 14 00:09:14 2013 fong zhou
// Last update Sat Feb 16 19:32:59 2013 lyoma guillou
//

#ifndef OPERAND_HPP_
# define OPERAND_HPP_

# include <iostream>
# include <sstream>

# include "exception.hpp"

namespace Op
{
  enum eOperandType
    {
      NIL = 0,
      Int8 = 1 << 0,
      Int16 = 1 << 1,
      Int32 = 1 << 2,
      Float = 1 << 3,
      Double = 1 << 4
    };
  
  class IOperand
  {
  public:
    virtual std::string const & toString() const = 0;

    virtual int getPrecision() const = 0;
    virtual eOperandType getType() const = 0;

    virtual IOperand *operator+(const IOperand &) const = 0;
    virtual IOperand *operator-(const IOperand &) const = 0;
    virtual IOperand *operator*(const IOperand &) const = 0;
    virtual IOperand *operator/(const IOperand &) const = 0;
    virtual IOperand *operator%(const IOperand &) const = 0;

    virtual ~IOperand() {}
  };

  template<typename T>
  class Operand : public IOperand
  {
    eOperandType _type;
    std::string _string;
    T _value;
    
  public:
    Operand(eOperandType, std::string const &);
    Operand(Operand const &);
    virtual ~Operand();

    Operand & operator=(Operand const &);

    Operand * newOperand(std::string const &) const;
    IOperand * OperandResult(double, IOperand const &) const;
    virtual std::string const & toString() const;
    virtual int getPrecision() const;
    virtual eOperandType getType() const;
    T getValue() const;
    
    virtual IOperand *operator+(const IOperand &) const;
    virtual IOperand *operator-(const IOperand &) const;
    virtual IOperand *operator*(const IOperand &) const;
    virtual IOperand *operator/(const IOperand &) const;
    virtual IOperand *operator%(const IOperand &) const;
  };
};

template<typename T>
Op::Operand<T>::Operand(eOperandType type, std::string const & value) :
  _type(type), _string(value)
{
  int tmp;
  std::stringstream ss(value);
  
  if (type == Int8) {
    ss >> tmp;
    _value = tmp;
  }
  else
    ss >> _value;
}
  
template<typename T>
Op::Operand<T>::Operand(Operand const & copy) {
  _type = copy.getType();
  _string = copy.toString();
  _value = copy.getValue();    
}
  
template<typename T>
Op::Operand<T>::~Operand() {}

template<typename T>
Op::Operand<T> & Op::Operand<T>::operator=(Operand const & copy) {
  _type = copy.getType();
  return *this;
}
  
template<typename T>
std::string const & Op::Operand<T>::toString() const {
  return _string;
}

template<typename T>
int Op::Operand<T>::getPrecision() const {
  return _type;
}

template<typename T>
Op::eOperandType Op::Operand<T>::getType() const {
  return _type;
}
template<typename T>
T Op::Operand<T>::getValue() const {
  return _value;
}
  
template <typename T>
Op::Operand<T>* Op::Operand<T>::newOperand(std::string const & value) const {
  return (new Operand<T>(this->_type, value));
}
  
template<typename T>
Op::IOperand * Op::Operand<T>::OperandResult(double result, IOperand const & rhs) const {
  std::stringstream ss;

  ss << result;
  if (this->getType() > rhs.getType())
    return (new Operand<T>(_type, ss.str()));
  return static_cast<const Operand &>(rhs).newOperand(ss.str());
}
  
template<typename T>
Op::IOperand * Op::Operand<T>::operator+(const IOperand &rhs) const {
  std::stringstream ss(rhs.toString());
  double sum;
    
  ss >> sum;
  sum = _value + sum;
  return OperandResult(sum, rhs);
}

template<typename T>
Op::IOperand * Op::Operand<T>::operator-(const IOperand &rhs) const {
  std::stringstream ss(rhs.toString());
  double sum;
    
  ss >> sum;
  sum = _value - sum;
  return OperandResult(sum, rhs);
}

template<typename T>
Op::IOperand * Op::Operand<T>::operator*(const IOperand &rhs) const {
  std::stringstream ss(rhs.toString());
  double sum;
    
  ss >> sum;
  sum = _value * sum;
  return OperandResult(sum, rhs);
}

template<typename T>
Op::IOperand * Op::Operand<T>::operator/(const IOperand &rhs) const {  
  std::stringstream ss(rhs.toString());
  double sum;

  ss >> sum;
  if (sum == 0)
    throw Exception("division by 0");
  sum = _value / sum;
  return OperandResult(sum, rhs);
}
  
template<typename T>
Op::IOperand * Op::Operand<T>::operator%(const IOperand &rhs) const {
  std::stringstream ss(rhs.toString());
  int sum;
  int value = _value;
    
  ss >> sum;
  if (_type > Op::Int32 || rhs.getType() > Op::Int32)
    throw Exception("only decimals expected for mod");
  if (sum == 0)
    throw Exception("modulo by 0");
  sum = value % sum;
  ss.clear();
  ss << sum;


  
  if (this->getType() > rhs.getType())
    return (new Operand<T>(_type, ss.str()));
  return static_cast<const Operand &>(rhs).newOperand(ss.str());  
}

#endif
