//
// Cpu.hpp for  in /home/zhou_f//c++/abstract_vm
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Mon Feb 11 17:09:29 2013 fong zhou
// Last update Thu Feb 14 16:19:26 2013 fong zhou
//

#ifndef CPU_HPP_
# define CPU_HPP_

# include <iostream>
# include <list>
# include <map>

# include "Operand.hpp"

class Cpu
{
  std::list<Op::IOperand *> _operands;
  std::map<std::string, void(Cpu::*)(void)> _instructs;  
  std::map<Op::eOperandType, Op::IOperand * (Cpu::*)(const std::string &)> _funcs;
    
public:
  Cpu();
  Cpu(Cpu const &);
  ~Cpu();

  Cpu & operator=(Cpu const &);

  std::list<Op::IOperand *> getOperands() const;
  void execute(std::string const &);
  void execute(std::string const &, Op::eOperandType const &, std::string const &);
  
  Op::IOperand * createOperand(Op::eOperandType, const std::string &);
  
private:
  void push(Op::IOperand *);
  void assert(Op::IOperand *);
  void dump() const;
  void print() const;
  
  void pop();
  void add();
  void sub();
  void mul();
  void div();
  void mod();

  void exit();
    
  Op::IOperand * createInt8(const std::string &);
  Op::IOperand * createInt16(const std::string &);
  Op::IOperand * createInt32(const std::string &);
  Op::IOperand * createIntFloat(const std::string &);
  Op::IOperand * createIntDouble(const std::string &);    
};

#endif
