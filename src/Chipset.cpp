//
// Chipset.cpp for  in /home/zhou_f//c++/abstract_vm
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Mon Feb 11 17:09:06 2013 fong zhou
// Last update Mon Feb 18 02:17:41 2013 lyoma guillou
//

#include <fstream>

#include "Operand.hpp"
#include "Chipset.hpp"
#include "Cpu.hpp"

static const struct IO::s_conv	g_types[]=
  {
    { "int8", Op::Int8 },
    { "int16", Op::Int16 },
    { "int32", Op::Int32 },
    { "float", Op::Float },
    { "double", Op::Double },
    { "", Op::NIL}
  };

static const struct IO::s_cmd	g_stnx[]=
  {
    
    { "push", Op::Int8 | Op::Int16 | Op::Int32 | Op::Float | Op::Double, "", 0 },
    { "assert", Op::Int8 | Op::Int16 | Op::Int32 | Op::Float | Op::Double, "", 0 },
    { "pop", Op::NIL, "", 0 },
    { "dump", Op::NIL, "", 0 },
    { "add", Op::NIL, "", 0 },
    { "sub", Op::NIL, "", 0 },
    { "mul", Op::NIL, "", 0 },
    { "div", Op::NIL, "", 0 },
    { "mod", Op::NIL, "", 0 },
    { "print", Op::NIL, "", 0 },
    { "exit", Op::NIL, "", 0 },
    { "", Op::NIL, "", 0 }
  };

IO::Chipset::Chipset() {}
IO::Chipset::Chipset(IO::Chipset const &copy) { this->cmds = copy.cmds; }
IO::Chipset::~Chipset() {}

bool IO::Chipset::error_check(struct IO::s_cmd exec)
{
  int i = 0;

  while ("" != g_stnx[i].cmd && exec.cmd != g_stnx[i].cmd)
    ++i;
  if (("" == g_stnx[i].cmd) || (exec.type != Op::NIL && !(exec.type & g_stnx[i].type)))
    return false;
  return true;
}

Op::eOperandType IO::Chipset::get_type(std::string & typeName) const
{
  int i = 0;

  while ("" != g_types[i].name && typeName.c_str() != g_types[i].name)
    ++i;
  return g_types[i].type;
}

bool IO::Chipset::get_input(std::string & buff, unsigned int & line)
{
  struct IO::s_cmd cmd = { "", Op::NIL, "", line };
  std::istringstream iss(buff);
  std::stringstream ss;
  std::string val;
  unsigned int word = 0;
  unsigned pos = 0;

  ss << line;
  do {
    iss >> val;
    if (std::string::npos != val.find(";;") || (!word && std::string::npos != val.find("exit")))
      return false;
    pos = val.find("(");
    if (pos > val.size())
      cmd.cmd = val;
    else
      {
	std::string tmp = val.substr(0, pos);
	cmd.type = get_type(tmp);
	if (Op::NIL != cmd.type)
	  {
	    std::string arg = val.substr(pos + 1);
	    cmd.arg = arg.substr(0, arg.find(")"));
	  }
	else
	  throw Exception(ss.str() + ": Invalid argument type: \"" + tmp + "\"");
      }
    ++word;
  } while (iss.good());
  if (true != error_check(cmd))
    throw Exception(ss.str() + ": Syntax Error. Unknown command: \"" + cmd.cmd + "\"");
  this->cmds = cmd;
  ss.str("");
  return true;
}

void IO::Chipset::get_stream(char const *file_name)
{
  IO::eIOFlag flag = IO::IOS;
  std::ifstream file;
  std::string buff;
  unsigned int line = 0;

  try
    {
      if (NULL != file_name)
	{
	  file.open(file_name);
	  if (!file.is_open())
	    throw Exception("No such file");
	  flag = IO::FILE;
	}
      while ((flag == IO::IOS) ? std::getline(std::cin, buff) : (file.good() && std::getline(file, buff)))
	{
	  (flag == IO::IOS) ? (line = 1) : ++line;
	  if (false == get_input(buff, line))
	    break;
	  execute_cmd();
	}
      if (IO::FILE == flag)
	file.close();
    }
  catch (Exception & e) 
    {
      std::cerr << "Exception caught: " << ((IO::FILE == flag) ? file_name : "Standard input") 
		<< " at line " << e.what() << std::endl;
    }
}

void IO::Chipset::execute_cmd()
{
  if (this->cmds.type != Op::NIL)
    this->_cpu.execute(this->cmds.cmd, static_cast<Op::eOperandType>(this->cmds.type), this->cmds.arg);
  else
    this->_cpu.execute(this->cmds.cmd);
}

IO::Chipset & IO::Chipset::operator=(Chipset const &other)
{
  this->cmds = other.cmds;
  return *this;
}
