//
// Cpu.cpp for  in /home/zhou_f//c++/abstract_vm
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Mon Feb 11 17:10:09 2013 fong zhou
// Last update Sat Feb 16 11:18:28 2013 fong zhou
//

#include <stdint.h>

#include "Cpu.hpp"
#include "exception.hpp"

Cpu::Cpu() {
  this->_funcs[Op::Int8] = &Cpu::createInt8;
  this->_funcs[Op::Int16] = &Cpu::createInt16;
  this->_funcs[Op::Int32] = &Cpu::createInt32;
  this->_funcs[Op::Float] = &Cpu::createIntFloat;
  this->_funcs[Op::Double] = &Cpu::createIntDouble;

  this->_instructs["pop"] = &Cpu::pop;
  this->_instructs["add"] = &Cpu::add;
  this->_instructs["sub"] = &Cpu::sub;  
  this->_instructs["mul"] = &Cpu::mul;
  this->_instructs["div"] = &Cpu::div;
  this->_instructs["mod"] = &Cpu::mod;
}

Cpu::Cpu(Cpu const &copy) {
  _operands = copy.getOperands();
}

Cpu::~Cpu() {
}

Cpu & Cpu::operator=(Cpu const &copy) {
  _operands = copy.getOperands();
  return *this;
}

std::list<Op::IOperand *> Cpu::getOperands() const {
  return this->_operands;
}


void Cpu::execute(std::string const & instruct) {
  if (instruct == "dump")
    dump();
  else if (instruct == "print")
    print();
  else
    (this->*_instructs[instruct])();
}

void Cpu::execute(std::string const & instruct,
		  Op::eOperandType const & type,
		  std::string const & value) {
  if (instruct == "push")
    push(createOperand(type, value));
  else if (instruct == "assert")
    assert(createOperand(type, value));
}

Op::IOperand * Cpu::createOperand(Op::eOperandType type, const std::string & value) {
  return (this->*_funcs[type])(value);
}

Op::IOperand * Cpu::createInt8(const std::string & value) {
  return (new Op::Operand<int8_t>(Op::Int8, value));
}

Op::IOperand * Cpu::createInt16(const std::string & value) {
  return (new Op::Operand<int16_t>(Op::Int16, value));
}

Op::IOperand * Cpu::createInt32(const std::string & value) {
  return (new Op::Operand<int32_t>(Op::Int32, value));
}

Op::IOperand * Cpu::createIntFloat(const std::string & value) {
  return (new Op::Operand<float>(Op::Float, value));
}

Op::IOperand * Cpu::createIntDouble(const std::string & value) {
  return (new Op::Operand<double>(Op::Double, value));
}

void Cpu::push(Op::IOperand * op) {
  _operands.push_front(op);
}

void Cpu::assert(Op::IOperand * op) {
  if (_operands.front()->getType() != op->getType() ||
      _operands.front()->toString() != op->toString())
    throw Exception("values does not match");
}

void Cpu::dump() const {
  // for_each(begin, end, Op::function);
  for (std::list<Op::IOperand *>::const_iterator it = _operands.begin(); it != _operands.end(); it++)
    std::cout << (*it)->toString() << std::endl;
}

void Cpu::print() const {
  char c;
  int tmp;
  std::istringstream ss;  
  
  if (_operands.front()->getType() != Op::Int8)
    throw Exception("Not a char type");
  ss.str(_operands.front()->toString());
  ss >> tmp;
  c = tmp;
  std::cout << c << std::endl;
  
}

void Cpu::pop() {
  if (_operands.empty())
    throw Exception("stack empty");
  delete _operands.front();
  _operands.pop_front();
}

void Cpu::add(){
  Op::IOperand *nb1;
  Op::IOperand *res;
  
  if (_operands.size() < 2)
    throw Exception("not enough elements for add");

  nb1 = _operands.front();
  _operands.pop_front();
  res =  *_operands.front() + *nb1;
  delete nb1;
  delete _operands.front();
  _operands.pop_front();
  _operands.push_front(res);
}

void Cpu::sub() {
  Op::IOperand *nb1;
  Op::IOperand *res;
  
  if (_operands.size() < 2)
    throw Exception("not enough elements for sub");

  nb1 = _operands.front();
  _operands.pop_front();
  res =  *_operands.front() - *nb1;
  delete nb1;
  delete _operands.front();
  _operands.pop_front();
  _operands.push_front(res);
}

void Cpu::mul() {
  Op::IOperand *nb1;
  Op::IOperand *res;
  
  if (_operands.size() < 2)
    throw Exception("not enough elements for mul");
  
  nb1 = _operands.front();
  _operands.pop_front();
  res =  *_operands.front() * *nb1;
  delete nb1;
  delete _operands.front();
  _operands.pop_front();
  _operands.push_front(res);
}

void Cpu::div() {
  Op::IOperand *nb1;
  Op::IOperand *res;

  if (_operands.size() < 2)
    throw Exception("not enough elements for div");
  
  nb1 = _operands.front();
  _operands.pop_front();
  res =  *_operands.front() / *nb1;
  delete nb1;
  delete _operands.front();
  _operands.pop_front();
  _operands.push_front(res);
}

void Cpu::mod() {
  Op::IOperand *nb1;
  Op::IOperand *res;

  if (_operands.size() < 2)
    throw Exception("not enough elements for mod");
  
  nb1 = _operands.front();
  _operands.pop_front();
  res =  *_operands.front() % *nb1;
  delete nb1;
  delete _operands.front();
  _operands.pop_front();
  _operands.push_front(res);
}
