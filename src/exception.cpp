//
// exception.cpp for  in /home/zhou_f//c++/abstract_vm/tmp
// 
// Made by fong zhou
// Login   <zhou_f@epitech.net>
// 
// Started on  Tue Feb 12 13:55:46 2013 fong zhou
// Last update Sat Feb 16 10:49:30 2013 fong zhou
//

#include <exception>
#include "exception.hpp"

Exception::Exception(std::string const & msg) throw() : _e(msg)
{}

Exception::Exception(Exception const & copy) throw() : _e(copy.what())
{}

Exception::~Exception() throw()
{}

Exception & Exception::operator=(Exception const & copy) {
  _e = copy.what();
  return *this;
}

const char * Exception::what() const throw() {
  return _e.c_str();
}




